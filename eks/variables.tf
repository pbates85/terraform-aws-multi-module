################################################################################
# Cluster Variables
################################################################################
variable "EKS_CLUSTER_NAME" {
  type = string
  default = "default-cluster-test"
}

variable "EKS_VERSION" {
  default = 1.25
}

variable "VPC_ID" {}

variable "PRIVATE_SUBNET_IDS" {}


variable "aws_region" {
  default = "us-east-2"
}

variable "cluster_version" {
  default     = "1.25"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t2.medium"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 2
  description = "EKS node count"
}
