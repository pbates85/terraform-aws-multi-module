
module "eks" {
  source = "terraform-aws-modules/eks/aws"
  cluster_name                    = var.EKS_CLUSTER_NAME
  cluster_version                 = var.EKS_VERSION
  vpc_id                          = var.VPC_ID
  subnet_ids                      = var.PRIVATE_SUBNET_IDS
  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
  }

  # Extend cluster security group rules
  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  # Extend node-to-node security group rules
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  eks_managed_node_groups = {
    staging_node_group = {
      # required for respecting disk size
      launch_template_name = ""
      disk_size            = 50
      min_size             = 1
      max_size             = var.instance_count
      desired_size         = var.instance_count
      instance_types       = [var.instance_type]
    }
  }
}
