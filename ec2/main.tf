data "aws_availability_zones" "available" {}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*-x86_64-gp2"]
  }
}

data "aws_ami" "amazon_linux_23" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023*-x86_64"]
  }
}

locals {
  name   = var.NAME
  region = "us-east-2"
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)
  tags = {
    Name       = local.name
    Example    = local.name
  }
}

module "ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name = local.name

  instance_type          = var.INSTANCE_TYPE
  key_name               = var.KEY_NAME
  monitoring             = true
  vpc_security_group_ids = var.SG_IDS
  subnet_id              = var.SUBNET_ID
  associate_public_ip_address = var.PUBLIC_IP
  create_iam_instance_profile = true
  iam_role_description        = "IAM role for EC2 instance"
  iam_role_policies = {
    AdministratorAccess = "arn:aws:iam::aws:policy/AdministratorAccess"
  }
  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 50
    },
  ]
  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp3"
      volume_size = 5
      throughput  = 200
      encrypted   = true
      kms_key_id  = aws_kms_key.this.arn
    }
  ]
  tags = {
    Terraform   = "true"
    Environment = "bastion"
  }

}

###   Supporting Resoruces    ###

resource "aws_kms_key" "this" {
}