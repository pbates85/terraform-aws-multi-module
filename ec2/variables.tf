variable "NAME" {
  type = string
  default = "default-name-ec2"
}

variable "INSTANCE_TYPE" {
  type = string
  default = "t2.micro"
}

variable "SUBNET_ID" {}

variable "SG_IDS" {}

variable "KEY_NAME" {}

variable "PUBLIC_IP" {
  type = bool
  default = true
}