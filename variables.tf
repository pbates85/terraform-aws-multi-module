

variable "aws_region" {
  default     = "us-east-2"
  description = "AWS region"
}

variable "AWS_ACCESS_KEY_ID" {
  description = "AWS access key ID"
  default = ""
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS secret access key"
  default = ""
}