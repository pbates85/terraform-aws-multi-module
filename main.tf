# root module

provider "aws" {
  region     = var.aws_region
}

data "aws_availability_zones" "region_availability_zones" {
  state = "available"
}

# https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#initialize-a-terraform-state-as-a-backend-by-using-gitlab-cicd
terraform {
  backend "http" {
  }
}

###   EKS CLUSTER WITH ISOLATED VPC   ###
#module "vpc_staging" {
#  source = "./vpc"
#  vpc_name = "staging-vpc"
#  vpc_cidr            = "10.2.0.0/16"
#  private_subnet_cidrs = ["10.2.1.0/24", "10.2.2.0/24", "10.2.3.0/24"]
#  public_subnet_cidrs  = ["10.2.101.0/24", "10.2.102.0/24", "10.2.103.0/24"]
#  azs                   = ["us-east-2a", "us-east-2b", "us-east-2c"]
#  PRIVATE_TAGS = {
#    "kubernetes.io/cluster/${module.staging-eks.cluster_name}" = "shared"
#    "kubernetes.io/role/elb"                                          = 1
#  }
#  PUBLIC_TAGS = {
#    "kubernetes.io/cluster/${module.staging-eks.cluster_name}" = "shared"
#    "kubernetes.io/role/internal-elb"                                 = 1
#  }
#}

#module "staging-eks" {
#  source = "./eks"
#  EKS_CLUSTER_NAME   = "Staging-Cluster-Test"
#  VPC_ID             = module.vpc_staging.vpc_id
#  PRIVATE_SUBNET_IDS = module.vpc_staging.private_subnets
#}


###    S3    ###
#module "s3_staging" {
#  source = "./s3"
#  s3_bucket_name = "terraform-staging-bucket-593428175"
#}

###   LAMBDA    ###
#module "lambda-role" {
#  source = "./iam"
#}

###   ECR Repo   ####
#module "ecr_staging" {
#  source = "./ecr"
#  repository_name = "staging-ecr-5395892"
#}

###   Bastion RDS   ###
module "vpc_bastion_rds" {
  source = "./vpc"
  vpc_name = "bastion-vpc"
  vpc_cidr            = "10.3.0.0/16"
  DATABASE_SUBNET = true
  PRIVATE_TAGS = {
    "vpc-name": "bastion-vpc-public"
  }
  PUBLIC_TAGS = {
    "vpc-name": "bastion-vpc-public"
  }
}

module "sg_bastion" {
  source = "./securityGroup"
  VPC_ID = module.vpc_bastion_rds.vpc_id
  EGRESS_RULES = ["all-all"]
  INGRESS_CIDR = []
  INGRESS_RULES = []
  INGRESS_WITH_CIDR = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "Bastion access CIDR"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      description = "MySQL access from within VPC"
      cidr_blocks = module.vpc_bastion_rds.vpc_cidr_block
    },
  ]
}

module "bastion" {
  source = "./ec2"
  KEY_NAME = "test-bastion"
  SG_IDS = [module.sg_bastion.security_group_id]
  SUBNET_ID = element(module.vpc_bastion_rds.public_subnets, 0)
}

#module "mysql_rds" {
#  source = "./rds"
#
#  INSTANCE_CLASS = "db.t3.micro"
#  SG_GROUP_ID = [module.sg_bastion.security_group_id]
#  SUBNET_GROUP_ARN = module.vpc_bastion_rds.database_subnet_group
#  DBNAME = "terraformtest"
#  USERNAME = "rdstfuser"
#}

