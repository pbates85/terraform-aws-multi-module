

variable "NAME" {
  type = string
  default = "mysql-default-db-name"
}

variable "INSTANCE_CLASS" {}

variable "ENGINE" {
  type = string
  default = "mysql"
}

variable "ENGINE_VERSION" {
  type = string
  default = "8.0"
}

variable "FAMILY" {
  type = string
  default = "mysql8.0"
}

variable "MAJOR_ENGINE_VERSION" {
  type = string
  default = "8.0"
}

variable "ALLOCATED_STORAGE" {
  type = number
  default = 20
}

variable "MAX_STORAGE" {
  type = number
  default = 100
}

variable "DBNAME" {
  type = string
  default = "testdb"
}

variable "USERNAME" {
  type = string
  default = "testuser"
}

variable "PORT" {
  type = number
  default = 3306
}

variable "MULTIAZ" {
  type = bool
  default = false
}

variable "SUBNET_GROUP_ARN" {}

variable "SG_GROUP_ID" {}

variable "PERFORMANCE" {
  type = bool
  default = false
}