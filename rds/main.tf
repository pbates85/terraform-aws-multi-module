
data "aws_availability_zones" "available" {}

locals {
  name   = var.NAME
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    Name       = local.name
  }
}

module "mysql-db" {
  source = "terraform-aws-modules/rds/aws"
  identifier = local.name

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine               = var.ENGINE
  engine_version       = var.ENGINE_VERSION
  family               = var.FAMILY # DB parameter group
  major_engine_version = var.MAJOR_ENGINE_VERSION      # DB option group
  instance_class       = var.INSTANCE_CLASS

  allocated_storage     = var.ALLOCATED_STORAGE
  max_allocated_storage = var.MAX_STORAGE

  db_name  = var.DBNAME
  username = var.USERNAME
  port     = var.PORT

  ###   NETWORK   ###
  multi_az               = var.MULTIAZ
  db_subnet_group_name  = var.SUBNET_GROUP_ARN
  vpc_security_group_ids = var.SG_GROUP_ID


  performance_insights_enabled          = var.PERFORMANCE
  performance_insights_retention_period = 7
  create_monitoring_role                = true
  monitoring_interval                   = 60

  maintenance_window              = "Mon:00:00-Mon:03:00"
  backup_window                   = "03:00-06:00"
  enabled_cloudwatch_logs_exports = ["general"]
  create_cloudwatch_log_group     = true
  blue_green_update = {
    enabled = true
  }


  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]

}