variable "repository_name" {
  description = "The name of the ECR repository."
  type        = string
  default     = "private-example"
}

variable "repository_access_role_arns" {
  description = "The ARNs of IAM roles with read-write access to the repository."
  type        = list(string)
  default     = ["arn:aws:iam::012345678901:role/terraform"]
}

variable "repository_lifecycle_policy" {
  description = "The lifecycle policy for the repository."
  type        = string
  default     = <<EOF
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Keep last 30 images",
      "selection": {
        "tagStatus": "tagged",
        "tagPrefixList": ["v"],
        "countType": "imageCountMoreThan",
        "countNumber": 30
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
EOF
}

variable "tags" {
  description = "Additional tags for the resources."
  type        = map(string)
  default     = {
    Terraform = "true"
  }
}
