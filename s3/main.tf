
module "s3" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = var.s3_bucket_name
  acl    = var.s3_bucket_acl

  control_object_ownership = var.s3_control_object_ownership
  object_ownership         = var.s3_object_ownership

  versioning = {
    enabled = var.s3_versioning_enabled
  }

  force_destroy = var.s3_force_destroy

#  attach_elb_log_delivery_policy = var.s3_attach_elb_log_delivery_policy
}