variable "s3_bucket_name" {
  description = "Name of the S3 bucket"
  type        = string
  default     = "s3-bucket-terraform-module"
}

variable "s3_bucket_acl" {
  description = "Access control list (ACL) for the S3 bucket"
  type        = string
  default     = "private"
}

variable "s3_control_object_ownership" {
  description = "Flag to control object ownership in S3 bucket"
  type        = bool
  default     = true
}

variable "s3_object_ownership" {
  description = "Type of object ownership in S3 bucket"
  type        = string
  default     = "ObjectWriter"
}

variable "s3_versioning_enabled" {
  description = "Flag to enable versioning for the S3 bucket"
  type        = bool
  default     = true
}

variable "s3_force_destroy" {
  description = "Flag to allow deletion of a non-empty S3 bucket"
  type        = bool
  default     = true
}

variable "s3_attach_elb_log_delivery_policy" {
  description = "Flag to attach an ELB log delivery policy to the S3 bucket"
  type        = bool
  default     = true
}
