# VPC using remote module from terraform registry

data "aws_availability_zones" "available" {}


locals {
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)
  }

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.vpc_name
  cidr   = var.vpc_cidr
  azs                 = local.azs
  private_subnets     = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k)]
  public_subnets      = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 4)]
  database_subnets    = [for k, v in local.azs : cidrsubnet(var.vpc_cidr, 8, k + 8)]

  enable_nat_gateway = true
  single_nat_gateway  = true
  one_nat_gateway_per_az = false
  enable_vpn_gateway = true

  create_database_subnet_group = var.DATABASE_SUBNET

  public_subnet_tags = var.PUBLIC_TAGS
  private_subnet_tags = var.PRIVATE_TAGS

}