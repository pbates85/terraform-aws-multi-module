

variable "vpc_name" {
  description = "Override value for the VPC name"
  default     = "vpc-using-tf-registry"  # Provide the desired override value here
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  default     = "10.0.0.0/16"  # Provide the desired default CIDR block
}

variable "DATABASE_SUBNET" {
  type = bool
  default = false
}

variable "PUBLIC_TAGS" {}
variable "PRIVATE_TAGS" {}