output "lambda_role_arn" {
  value = aws_iam_role.lambda_role.arn
}

output "lambda_role_name" {
  value = aws_iam_role.lambda_role.name
}

output "iam_policy_arn" {
  value = aws_iam_policy.iam_policy_for_lambda.arn
}

output "iam_policy_name" {
  value = aws_iam_policy.iam_policy_for_lambda.name
}