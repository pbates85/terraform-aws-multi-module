

resource "aws_lambda_function" "terraform_lambda_func" {
  image_uri        = var.LAMBDA_IMAGE_URI
  function_name    = var.LAMBDA_FUNCTION_NAME
  role             = var.LAMBDA_ROLE_ARN
  handler          = var.LAMBDA_HANDLER
  runtime          = var.LAMBDA_RUNTIME
  package_type     = var.LAMBDA_PACKAGE_TYPE

  environment {
    variables = {
      aws_access_key_id     = var.AWS_ACCESS_KEY_ID
      aws_secret_access_key = var.AWS_SECRET_ACCESS_KEY
    }
  }
}