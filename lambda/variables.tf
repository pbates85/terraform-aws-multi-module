variable "LAMBDA_IMAGE_URI" {
  description = "URI of the Lambda function image"
  default = "161660187510.dkr.ecr.us-east-2.amazonaws.com/ais-data-repo:latest"
}

variable "LAMBDA_FUNCTION_NAME" {
  description = "Name of the Lambda function"
  type = string
}

variable "LAMBDA_ROLE_ARN" {
  description = "ARN of the IAM role for the Lambda function"
}

variable "LAMBDA_HANDLER" {
  description = "Handler of the Lambda function"
  default = "index.lambda_handler"
}

variable "LAMBDA_RUNTIME" {
  description = "Runtime for the Lambda function"
  default = "java11"
}

variable "LAMBDA_PACKAGE_TYPE" {
  description = "Package type for the Lambda function"
  default = "Image"
}

variable "AWS_ACCESS_KEY_ID" {
  description = "AWS access key ID"
  default = ""
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS secret access key"
  default = ""
}