
variable "NAME" {
  type = string
  default = "securityGroupTester"
}

variable "VPC_ID" {}

variable "INGRESS_WITH_CIDR" {}
variable "INGRESS_CIDR" {}
variable "INGRESS_RULES" {}
variable "EGRESS_RULES" {}