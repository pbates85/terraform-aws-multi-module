
module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = var.NAME
  description = "Security group for example usage with EC2 instance"
  vpc_id      = var.VPC_ID

  ingress_with_cidr_blocks = var.INGRESS_WITH_CIDR
  ingress_cidr_blocks = var.INGRESS_CIDR
  ingress_rules       = var.INGRESS_RULES
  egress_rules        = var.EGRESS_RULES

  tags = {
    Name       = var.NAME
  }
}